<?php 

class Person {
	
	public $firstName;
	public $middleName;
	public $lastName;

	//A constructor is used during the creation of an object
	public function __construct($firstName, $middleName, $lastName){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	//methods of function

	public function printName(){
		return "Your full name is $this->firstName $this->middleName $this->lastName.";
	}

};

$person = new Person('Senku', 'Ishigami', 'Gojo');









class Developer extends Person{
	

	public function printName(){
		return "Your full name is $this->firstName $this->middleName $this->lastName and you are a developer.";
	}

}
$developer = new Developer('John', 'Finch', 'Smith');

class Engineer extends Person{
	

	public function printName(){
		return "Your are an engineer named $this->firstName $this->middleName $this->lastName.";
	}

}

$engineer = new Engineer('Harold', 'Myers', 'Reese');

 ?>
