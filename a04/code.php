<?php 
// $hello = 'Hello world!';
class Building {
	//if the access modifier of the property is private, you cannot directly access its value

	//if the access modifier is private the child class won't inherit the properties
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	//getter function of property name
	public function getName(){
		return "The name of the building is $this->name.";
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getFloors(){
		return "The $this->name has $this->floors floors.";
	}

	public function getAddress(){
		return "The $this->name is located at $this->address.";
	}

	public function getNewName(){
		return "The name of the building has been changed to $this->name.";
	}




}

class Condominium extends Building{

	public function getNewName(){
		return "The name of the condominium has been changed to $this->name.";
	}

	

}


$building = new Building('Caswyn Building', 8, 'Timog Avenue, Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
















class Drink {
	protected $name;

	public function __construct($name){
		$this->name = $name;
	}

	//getter for the name property
	public function getName(){
		return $this->name;
	}

	//setter for the name property
	public function setName($name){
		$this->name = $name;
	}
}

$milk = new Drink('Alaska');

class Coffee extends Drink {

}

$kopiko = new Coffee('Kopiko');


 ?>
