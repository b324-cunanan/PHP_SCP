<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<title>S04:Access Modifiers and Encapsulation</title>
	</head>

	<body>
		<h1>Building</h1>
		
		<p><?php echo $building->getName(); ?></p>
		

		<p><?php echo $building->getFloors(); ?></p>

		<p><?php echo $building->getAddress(); ?></p>

		<p><?php $building->setName('Caswynn Complex'); ?></p>

		<p><?php echo $building->getNewName(); ?></p>




		<h1>Condominium</h1>

		<p><?php echo $condominium->getName(); ?></p>
		

		<p><?php echo $condominium->getFloors(); ?></p>

		<p><?php echo $condominium->getAddress(); ?></p>

		<p><?php $condominium->setName('Enzo Tower'); ?></p>

		<p><?php echo $condominium->getNewName(); ?></p>

	

		

		
		
	</body>
</html>
