<?php
session_start();

$registeredUser = array(
    "johnsmith@gmail.com" => "1234"
);

$email = isset($_POST["email"]) ? $_POST["email"] : '';
$password = isset($_POST["password"]) ? $_POST["password"] : '';

if (isset($registeredUser[$email]) && $registeredUser[$email] === $password) {
    header("Location: login.php?email=" . $email);
    exit;
} else {
    header("Location: index.php");
    exit;
}

if (isset($_POST["Logout"])) {
    session_destroy();
    header("Location: index.php");
    exit;
}
?>






