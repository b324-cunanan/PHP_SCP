<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<title>S03: Classes and Objects</title>
	</head>
	<body>
		<h1>Objects from Varibale</h1>
		<p><?php echo $buildingObj->name; ?></p>
		<p><?php echo $buildingObj->address->city." ".$buildingObj->address->country; ?></p>
		<p><?php var_dump($buildingObj); ?></p>

		<h1>Object from Class</h1>
		<p><?php var_dump($building); ?></p>
		<p><?php echo $building->name; ?></p>
		<p><?php echo $building->printName(); ?></p>
		<p><?php echo $building->checkFloors(); ?></p>
		<p><?php echo $secondBuilding->printName(); ?></p>

		<h1>Inheritance</h1>
		<p><?php var_dump($condominium); ?></p>
		<p><?php echo $condominium->printName(); ?></p>
		<p><?php echo $condominium->checkFloors(); ?></p>
		<p><?php echo $condominium->checkZipCode(); ?></p>
	</body>
</html>
