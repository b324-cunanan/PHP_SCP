<?php  
	$tasks = ['get git', 'Bake HTML', 'EAT CSS', 'PHP'];

	if(isset($_GET['index'])){
		$indexGet = $_GET['index'];
		echo "The retrieved task from GET is $tasks[$indexGet]";
	}


	if(isset($_POST['index'])){
		$indexPOST = $_POST['index'];
		echo "The retrieved task from POST is $tasks[$indexPOST]";
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S05: Client-Server Communication - Discussion 1</title>
	</head>

	<body>
		<h1>Get Method:</h1>

		<form method="GET">

            <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

            <button type="submit">GET</button>

        </form>

        <p><?php echo $_GET['index'];?></p>
        <p><?php var_dump(isset($_GET['index']));?></p>

        <h1>POST Method:</h1>

        <form method="POST">

            <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

            <button type="submit">POST</button>

        </form>

        <p><?php echo $_POST['index'];?></p>
        <p><?php var_dump(isset($_POST['index']));?></p>
	</body>
</html>