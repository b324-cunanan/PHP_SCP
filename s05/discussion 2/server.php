<?php 
session_start();

class TaskList {
	
	public function addTask($description){
		$newTask = (object)[
			'description' => $description,
			'isFinished' => false
		];

		//validation whether the token session is existing or not
		if($_SESSION['tasks'] === null){
			$_SESSION['tasks'] = array();
		}

		array_push($_SESSION['tasks'], $newTask);


	}

	public function update($id, $description, $isFinished) {
    if (isset($_SESSION['tasks'][$id])) {
        $_SESSION['tasks'][$id]->description = $description;
        $_SESSION['tasks'][$id]->isFinished = $isFinished;
    }
}

	public function delete($id){
		array_splice($_SESSION['tasks'], $id, 1);
	}

	public function clear(){
		session_destroy();
	}
}

$tasklist = new TaskList();

if($_POST['action'] === 'add'){
	$tasklist->addTask($_POST['description']);
}else if($_POST['action'] === 'update'){
	$tasklist->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
}else if($_POST['action'] === 'delete'){
	$tasklist->delete($_POST['id']);
}else if($_POST['action'] === 'clear'){
	$tasklist->clear();
}



header('Location: ./index.php');
 ?>
